//
// Created by endlesskwazar on 12.11.18.
//

#ifndef DEMO_FS_IO_DATE_ALIST_ARRAYLIST_H
#define DEMO_FS_IO_DATE_ALIST_ARRAYLIST_H


template <typename T> struct ArrayList{
    int startCapacity  = 4;
    int currentIndex = 0;
    int currentCapacity = startCapacity;
    T* data = new T[startCapacity];

    void add(T item)
    {
        data[currentIndex++] = item;

        if(currentIndex == currentCapacity){
            currentCapacity *= 2;
            T* temp = new T[currentCapacity];
            for(int i = 0; i < currentIndex; i++)
                temp[i] = data[i];
            delete []data;
            this->data = temp;
        }
    }

    T getData(){
        T result[currentIndex];
        for(int i = 0; i < currentIndex; i++)
            result[i] = data[i];
        return result;
    }

};


#endif //DEMO_FS_IO_DATE_ALIST_ARRAYLIST_H
