//
// Created by endlesskwazar on 12.11.18.
//

#ifndef DEMO_FS_IO_DATE_ALIST_EMPLOYEE_H
#define DEMO_FS_IO_DATE_ALIST_EMPLOYEE_H


#include <ctime>

struct Employee {
    char* name;
    float sallary;
    time_t workedFrom;

    Employee();
    Employee(char* name, float sallary, time_t workedFrom);
};


#endif //DEMO_FS_IO_DATE_ALIST_EMPLOYEE_H
