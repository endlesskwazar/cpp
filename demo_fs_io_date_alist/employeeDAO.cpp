//
// Created by endlesskwazar on 12.11.18.
//

#include "employeeDAO.h"
#include <fstream>
#include <cstring>

void EmployeeDAO::addEmployee(const Employee &employee) {
    this->data->add(employee);
}

void EmployeeDAO::saveToFile() {
    std::ofstream db;
    db.open (this->filePath, std::ios::out | std::ios::trunc);
    for(int i = 0; i < this->data->currentIndex; i++){
        db << "###" << '\n';
        db << this->data->data[i].name << '\n';
        db << this->data->data[i].sallary << '\n';
        db << this->data->data[i].workedFrom << '\n';
        db << "@@@" << '\n';
    }
    db.close();
}

void EmployeeDAO::loadFromFile() {
    std::ifstream db;
    db.open(this->filePath, std::ios::in);

    std::string line;
    Employee* empl;
    int i = 0;

    if (db.is_open())
    {
        while ( getline (db,line) )
        {
            if(line == "###"){
                empl = new Employee;
                continue;
            }

            char *cstr;
            switch (i){
                case 0:

                    cstr = new char[line.length() + 1];
                    strcpy(cstr, line.c_str());
                    empl->name = cstr;
                    break;
                case 1:
                    empl->sallary = std::stof(line);
                    break;
                case 2:
                    empl->workedFrom = time(0);
                    break;
            }
            i++;

            if(line == "@@@"){
                this->addEmployee(*empl);
                empl = new Employee();
                i = 0;
            }

        }


        }
        db.close();

}