//
// Created by endlesskwazar on 12.11.18.
//

#ifndef DEMO_FS_IO_DATE_ALIST_EMPLOYEEDAO_H
#define DEMO_FS_IO_DATE_ALIST_EMPLOYEEDAO_H


#include "employee.h"
#include "arrayList.h"

struct EmployeeDAO {

    ArrayList<Employee> *data = new ArrayList<Employee>();
    char* filePath = "db.txt";

    void addEmployee(const Employee& employee);
    void loadFromFile();
    void saveToFile();
    ArrayList<Employee>* getData();
};


#endif //DEMO_FS_IO_DATE_ALIST_EMPLOYEEDAO_H
