#include <iostream>
#include <string>
#include <fstream>
#include <limits>

using namespace std;

string taskslist = "tasklist";

struct task
{
	string taskname;
	int date[3];
};

void recordtasklist()
{
	ofstream out;
	out.open(taskslist, ofstream::ate | ofstream::app);
	if (out.is_open())
	{
		out << "something" << endl;
	}
	out.close();
}

void recordintaskslist(task N)
{
	ofstream out;
	out.open(taskslist, ofstream::ate | ofstream::app);
	if (out.is_open())
	{
		out << N.taskname << endl;
	}
	out.close();
}

void recordedintaskslist()
{
	string line;
	cout << endl << "Cписок всех задач: " << endl;
	ifstream in(taskslist);
	if (in.is_open())
	{
		int g = 1;
		while (getline(in, line))
		{
			cout << " " << g << " " << line << endl;
			g++;
		}
		cout << "\n Любое другое число - не открывать ничего \n";
	}
	in.close();
}

void checkFile(bool &checkfile)
{
	checkfile = true;
	ifstream file;
	file.open(taskslist);
	if (!file)
	{
		cout << "There is no tasks";
		checkfile = false;
	}
}

void GetTask(task& N, bool &datefail)
{
	if (datefail == false)
	{
		cout << "Название задачи: ";
		cin.ignore();
		getline(cin, N.taskname);
	}
	cout << "Введите день, месяц, год: ";

	//anti letters in date
	while (!(cin >> N.date[0]) || !(cin >> N.date[1]) || !(cin >> N.date[2]) || (cin.peek() != '\n'))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Input error! Retry input" << endl;
	}
	if (N.date[0] > 31 | N.date[1] > 12 | N.date[2] < 1)
	{
		cout << "\nIncorect date, retry: \n";
		datefail = true;
		GetTask(N, datefail);
	}
}

void recordtask(task N)
{
	ofstream out;
	string recorded = N.taskname + ".txt";
	out.open(recorded);
	if (out.is_open())
	{
		out << "Задача: " << N.taskname << "\t" << "Сделать до: " << N.date[0] << "." << N.date[1] << "." << N.date[2] << endl;
	}
	out.close();
}

void opentask(string& needtoopen, int &dontopen)
{
	dontopen = 1;
	string line;
	ifstream in(taskslist);
	if (in.is_open())
	{
		int g = 1;
		int K;
		cout << "\n Открыть задачу: ";
		while (!(cin >> K) || (cin.peek() != '\n'))
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Input error! Retry input" << endl;
		}
		while (getline(in, line))
		{
			g;
			if (g == K)
			{
				dontopen = 0;
				needtoopen = line;
			}
			g++;
		}
	}
	in.close();
}

void readtask(string needtoopen, int dontopen)
{
	if (dontopen == 0)
	{
		string line;
		string task = needtoopen + ".txt";
		ifstream in(task);
		if (in.is_open())
		{
			while (getline(in, line))
			{
				cout << endl << line << endl;
			}
		}
		in.close();
	}
}

void search(string enteredsearch, bool& searchsuccess)
{
	int lookingfor;
	int g = 1;
	string line;
	ifstream in(taskslist);
	if (in.is_open())
	{

		while (getline(in, line))
		{
			lookingfor = line.find(enteredsearch);

			if (lookingfor != -1)
			{
				cout << "Found the task №" << g << ": " << line << endl;
				searchsuccess = 1;
			}

			g++;
		}
		if (searchsuccess == 0)
		{
			cout << "There is nothing with this task";
		}
	}
	in.close();
}

int menu(task N, string needtoopen, int &dontopen)
{
	int taskselection;
	cout << " 1 - Создать задачу \n 2 - Просмотр всех задач \n 3 - Поиск по всем задачам \n 4 - Закрыть \n № ";
	cout << "Select task: ";

	//anti cin letters
	while (!(cin >> taskselection) || (cin.peek() != '\n'))
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Input error! Retry input" << endl;
	}

	if (taskselection < 5 && taskselection > 0)
	{
		switch (taskselection)
		{
		case (1):
		{
			bool datefail = false;
			GetTask(N, datefail);
			recordtask(N);
			recordintaskslist(N);
			break;
		}
		case (2):
		{
			bool checkfile;
			checkFile(checkfile);
			if (checkfile == true)
			{
				recordedintaskslist();
				opentask(needtoopen, dontopen);
				readtask(needtoopen, dontopen);
			}
			break;
		}
		case (3):
		{
			bool searchsuccess = 0;
			string enteredsearch;
			cout << "Looking for tasks: ";
			cin.ignore();
			getline(cin, enteredsearch);
			search(enteredsearch, searchsuccess);
			if (searchsuccess == 1)
			{
				opentask(needtoopen, dontopen);
				readtask(needtoopen, dontopen);
			}
			break;
		}
		case (4):
		{
			exit(0);
		}
		}
		cout << endl << endl;
	}
	else
		cout << "There is no " << taskselection << " task" << endl;
	return taskselection;
}

int main()
{
	setlocale(LC_ALL, "");
	task N;
	string needtoopen;
	int dontopen;
	menu(N, needtoopen, dontopen);
	return main();
}